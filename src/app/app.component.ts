import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/inicio', icon: 'home'},
    { title: 'Categorias', url: '/categoria', icon: 'document'},
    { title: 'Livros', url: '/livro', icon: 'document-text'},
    { title: 'Usuário', url: '/usuario', icon: 'man'},
    { title: 'Meta', url: "/meta", icon: 'checkmark' },
    { title: 'Login', url: '/login', icon: 'log-out'},
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
