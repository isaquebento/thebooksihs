import { Injectable } from '@angular/core';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  constructor(private usuarioService: UsuarioService) {
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
    if(!metas) {
      metas = [];
      localStorage.setItem('tbMetas', JSON.stringify(metas));
      localStorage.setItem('idEditarMeta', JSON.stringify(-1));
    }
  }

  salvar(meta: any) {
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
    meta.usuario = this.usuarioService.recuperarAutenticacao();
    if(meta.id) {
      let posicao = metas.findIndex(c=> c.id == meta.id);
      metas[posicao] = meta;
    } else {
      meta.id = new Date().getTime();
      metas.push(meta);
    }
    meta = this.atualizarClasse(meta);
    localStorage.setItem('tbMetas', JSON.stringify(metas));
    this.limparIdEditar();
  }

  atualizarClasse(meta:any){
    let agora:any = new Date();
    if (meta.dataFinal - agora < 0){
      if (meta.livrosLidos < meta.livrosTotal){
        meta.classe = "expired";
      }else{
        meta.classe = "finished";
      }
    }else{
      if (meta.livrosLidos >= meta.livrosTotal){
        meta.classe = "finished";
      }else{
        meta.classe = "pending";
      }
    }
    return meta;
  }

  limparIdEditar() {
    localStorage.setItem('idEditarMeta', JSON.stringify(-1));
  }

  buscarPorId(id: number) {
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
    let meta = metas.find(c => c.id == id);
    return meta;
  }

  listar() {
    this.contarLivrosConcluidos();
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
        metas = metas.filter(c => c.usuario.id == this.usuarioService.recuperarAutenticacao().id);
    return metas;
  }

  contarLivrosConcluidos(){
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    for (let i = 0; i < metas.length; i++){
      metas[i].livrosLidos = 0;
      for (let j = 0; j < livros.length; j++){
        if (livros[j].dataConcluido && livros[j].concluido == "1"){
          let dataLivro = new Date(livros[j].dataConcluido);

          let dataInicio = new Date(metas[i].dataInicio);
          let dataFinal = new Date(metas[i].dataFinal);
          if (dataInicio < dataLivro && dataFinal > dataLivro){
            metas[i].livrosLidos++;
            metas[i] = this.atualizarClasse(metas[i]);
          }
        }
      }
    }
    localStorage.setItem('tbMetas', JSON.stringify(metas));
  }

  excluir(meta: any) {
    let metas = JSON.parse(localStorage.getItem('tbMetas'));
    metas = metas.filter(c => c.id != meta.id);
    localStorage.setItem('tbMetas', JSON.stringify(metas));
  }

  recuperarIdEditar() {
    let id = JSON.parse(localStorage.getItem('idEditarMeta'));
    return id;
  }
}
