import { Injectable } from '@angular/core';
import { MetaService } from './meta.service';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class LivroService {

  constructor(private metaService: MetaService, private usuarioService: UsuarioService){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    if (!livros){
      livros = [];
      localStorage.setItem('tbLivros', JSON.stringify(livros));
      localStorage.setItem('idEditarLivro', JSON.stringify(-1));
    }
  }

  salvar(livro: any){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    if (livro.categoria){
      let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
      for (let i = 0; i < categorias.length; i++){
        if (livro.categoria == categorias[i].id){
          livro.categoria = categorias[i];
        }
      }
    }else{
      livro.categoria = {
        id:"0",
        nome:"",
      }
    }

    livro.usuario = this.usuarioService.recuperarAutenticacao();

    if (livro.id){
      let posicao = livros.findIndex(c => c.id == livro.id);
      console.log(posicao);
      livros[posicao] = livro;
    }else{
      livro.id = new Date().getTime();
      livros.push(livro);
    }
    localStorage.setItem('tbLivros', JSON.stringify(livros));
    this.metaService.contarLivrosConcluidos();
    this.limparIdEditar();
  }

  excluir(livro: any){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    livros = livros.filter(c => c.id != livro.id);
    localStorage.setItem('tbLivros', JSON.stringify(livros));
  }

  listar(){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
        livros = livros.filter(c => c.usuario.id == this.usuarioService.recuperarAutenticacao().id);
    return livros;
  }

  listarPorCategoria(categoria: any){
    let livros = this.listar();
        livros = livros.filter(c => c.categoria.id == categoria.id);
    return livros;
  }

  buscarPorId(id: number){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    let livro = livros.find(c => c.id == id);
    return livro;
  }
  
  limparIdEditar() {
    localStorage.setItem('idEditarLivro', JSON.stringify(-1));
  }
}
