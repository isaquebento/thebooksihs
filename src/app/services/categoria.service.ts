import { Injectable } from '@angular/core';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private usuarioService: UsuarioService) {
    let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
    if(!categorias) {
      categorias = [];
      localStorage.setItem('tbCategorias', JSON.stringify(categorias));
      localStorage.setItem('idEditarCategoria', JSON.stringify(-1));
    }
  }

  salvar(categoria: any) {
    let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
    categoria.usuario = this.usuarioService.recuperarAutenticacao();
    if(categoria.id) {
      let posicao = categorias.findIndex(c=> c.id == categoria.id);
      this.editarCategoriaLivros(categoria);
      categorias[posicao] = categoria;
    } else {
      categoria.id = new Date().getTime();
      categorias.push(categoria);
    }
    localStorage.setItem('tbCategorias', JSON.stringify(categorias));
    this.limparIdEditar();
  }

  limparIdEditar() {
    localStorage.setItem('idEditarCategoria', JSON.stringify(-1));
  }

  buscarPorId(id: number) {
    let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
    let categoria = categorias.find(c => c.id == id);
    return categoria;
  }

  listar() {
    let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
        categorias = categorias.filter(c => c.usuario.id == this.usuarioService.recuperarAutenticacao().id);
    return categorias;
  }

  excluir(categoria: any) {
    let categorias = JSON.parse(localStorage.getItem('tbCategorias'));
    this.addNull(categoria.id);
    categorias = categorias.filter(c => c.id != categoria.id);
    localStorage.setItem('tbCategorias', JSON.stringify(categorias));
  }

  editarCategoriaLivros(categoria: any){
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    for(let c = 0; c < livros.length; c++) {
      if (livros[c].categoria.id == categoria.id){
        livros[c].categoria = categoria;
      }
    }
    localStorage.setItem('tbLivros', JSON.stringify(livros));
  }

  addNull(id: number) {
    let livros = JSON.parse(localStorage.getItem('tbLivros'));
    for(let c = 0; c < livros.length; c++) {
      if (livros[c].categoria.id == id){
        livros[c].categoria = "null";
      }
    }
    localStorage.setItem('tbLivros', JSON.stringify(livros));
  }

  recuperarIdEditar() {
    let id = JSON.parse(localStorage.getItem('idEditarCategoria'));
    return id;
  }
}
