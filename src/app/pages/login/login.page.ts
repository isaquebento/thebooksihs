import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario = {
    email:null,
    senha: null,
  }

  public formGroup:FormGroup;

  constructor(private navController: NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private formBuilder: FormBuilder, private usuarioService: UsuarioService, public menuCtrl: MenuController){
    this.menuCtrl.enable(false);
    this.formGroup = this.formBuilder.group({
      'email':['', Validators.compose([
        Validators.required, Validators.email
      ])],
      'senha':['', Validators.compose([
        Validators.required
      ])]
    })
  }

  ngOnInit() {
    this.usuarioService.encerrarAutenticacao();
  }

  async submitForm(){
    this.usuario.email = this.formGroup.value.email;
    this.usuario.senha = this.formGroup.value.senha;
    if(this.usuarioService.autenticar(this.usuario.email, this.usuario.senha)){
      this.navController.navigateBack('/inicio');
    }else{
      this.exibirMensagem('E-mail ou senha incorreto');
      this.navController.navigateBack('/login')
    }
  }

  async exibirMensagem(mensagem){
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500,
    });
    toast.present();
  }

}
