import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { MetaService } from 'src/app/services/meta.service';

@Component({
  selector: 'app-meta',
  templateUrl: './meta.page.html',
  styleUrls: ['./meta.page.scss'],
})
export class MetaPage implements OnInit {
  metas: any = [];

  constructor(public alertController: AlertController, public toastController: ToastController, public metaService: MetaService, public navController: NavController,public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
  }
  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.metas = this.metaService.listar();
  }

  async excluirMeta(id: number) {
    let meta = this.metaService.buscarPorId(id);

    const alert = await this.alertController.create({
      header: 'Quer mesmo excluir?',
      message: meta.nome,
      buttons: [
        {
          text: 'Cancelar'
        }, {
          text: 'Quero',
          cssClass: 'danger',
          handler: () => {
            this.metaService.excluir(meta);
            this.metas = this.metaService.listar();
            this.exibirMensagem("Meta excluída com sucesso!");
          }
        }
      ]
    });
    await alert.present();
  }

  async exibirMensagem(mensagem: string) {
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500
    });
    toast.present();
  }

  async editarMeta(id: number) {
    localStorage.setItem('idEditarMeta', JSON.stringify(id));
    this.navController.navigateBack('/add-meta');
  }

  async lerDataInicio(id: number){
    let meta = this.metaService.buscarPorId(id);
    return meta.dataInicio.getTime();
  }
}
