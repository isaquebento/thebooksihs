import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { CategoriaService } from 'src/app/services/categoria.service';
import { LivroService } from 'src/app/services/livro.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-add-livro',
  templateUrl: './add-livro.page.html',
  styleUrls: ['./add-livro.page.scss'],
})
export class AddLivroPage implements OnInit {
  id: string;
  livro = {
    id: null,
    titulo: null,
    pag: null,
    categoria: null,
    autor: null,
    editora: null,
    concluido: null,
    dataConcluido: null,
    usuario: null,

    cor:null,
  }
  categorias:any = [];

  public formGroup: FormGroup;

  constructor(private navController:NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private categoriaService: CategoriaService, private livroService: LivroService, private usuarioService: UsuarioService, private formBuilder: FormBuilder, public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
    this.formGroup = this.formBuilder.group({
      'titulo':['', Validators.compose([
        Validators.required,
      ])],
      'pag':['', Validators.compose([
        Validators.required,
      ])],
      'categoria':['', Validators.compose([
        Validators.required,
      ])],
      'autor':['', Validators.compose([
        Validators.required,
      ])],
      'editora':['', Validators.compose([
        Validators.required,
      ])],
      'concluido':['', Validators.compose([
        Validators.required,
      ])],
    })
  }

  ngOnInit() {
    this.categorias = this.categoriaService.listar();
    
    this.id = localStorage.getItem('idEditarLivro');
    if (parseInt(this.id) != -1){
      this.livro = this.livroService.buscarPorId(parseInt(this.id));
      
      this.formGroup.get('titulo').setValue(this.livro.titulo);
      this.formGroup.get('pag').setValue(this.livro.pag);
      this.formGroup.get('categoria').setValue(this.livro.categoria.id);
      this.formGroup.get('autor').setValue(this.livro.autor);
      this.formGroup.get('editora').setValue(this.livro.editora);
      this.formGroup.get('concluido').setValue(this.livro.concluido);
    }
  }

  async submitForm(){
    this.livro.titulo = this.formGroup.value.titulo;
    this.livro.pag = this.formGroup.value.pag;
    this.livro.categoria = this.formGroup.value.categoria;
    this.livro.autor = this.formGroup.value.autor;
    this.livro.editora = this.formGroup.value.editora;
    this.livro.concluido = this.formGroup.value.concluido;

    if (this.livro.concluido == 1){
      this.livro.cor = "success";
      this.livro.dataConcluido = new Date();
    }else{
      this.livro.cor = "danger";
      this.livro.dataConcluido = null;
    }

    if (this.livro.pag <= 0){
      this.exibirMensagem('Falha ao salvar registro');
      this.navController.navigateBack('/livro');
      return;
    }

    this.livro.usuario = null;

    this.livroService.salvar(this.livro);
    this.exibirMensagem("Registro salvo com sucesso");
    this.navController.navigateBack('/livro')
  }

  async exibirMensagem(mensagem: string){
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500,
    });
    toast.present();
  }

}
