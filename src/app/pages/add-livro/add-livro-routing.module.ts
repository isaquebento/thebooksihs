import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLivroPage } from './add-livro.page';

const routes: Routes = [
  {
    path: '',
    component: AddLivroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLivroPageRoutingModule {}
