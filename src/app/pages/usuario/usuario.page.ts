import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  usuario = {
    id:null,
    nome:null,
    cpf:null,
    data:null,
    cel:null,
    email:null,
    senha:null,
    sexo:null,
  }
  public formGroup: FormGroup;

  constructor(private navController: NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private formBuilder: FormBuilder, private usuarioService: UsuarioService){
    this.formGroup = this.formBuilder.group({
      'nome':['', Validators.compose([
        Validators.required
      ])],
      'cpf':['', Validators.compose([
        Validators.required
      ])],
      'data':['', Validators.compose([
        Validators.required
      ])],
      'cel':['', Validators.compose([
        Validators.required
      ])],
      'email':['', Validators.compose([
        Validators.required, Validators.email
      ])],
      'senha':['', Validators.compose([
        Validators.required
      ])],
      'sexo':['', Validators.compose([
        Validators.required
      ])],
    })
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('usuarioAutenticado')) == null){
      this.navController.navigateBack('/login');
      return;
    }
    this.usuario = this.usuarioService.recuperarAutenticacao();
    this.formGroup.get('nome').setValue(this.usuario.nome);
    this.formGroup.get('cpf').setValue(this.usuario.cpf);
    this.formGroup.get('data').setValue(this.usuario.data);
    this.formGroup.get('cel').setValue(this.usuario.cel);
    this.formGroup.get('email').setValue(this.usuario.email);
    this.formGroup.get('senha').setValue(this.usuario.senha);
    this.formGroup.get('sexo').setValue(this.usuario.sexo);
  }
  
  async submitForm(){
    this.usuario.nome = this.formGroup.value.nome;
    this.usuario.cpf = this.formGroup.value.cpf;
    this.usuario.data = this.formGroup.value.data;
    this.usuario.cel = this.formGroup.value.cel;
    this.usuario.email = this.formGroup.value.email;
    this.usuario.senha = this.formGroup.value.senha;
    this.usuario.sexo = this.formGroup.value.sexo;

    let novoUsuario = {
      id: null,
      nome: this.formGroup.value.nome,
      email: this.formGroup.value.email,
      senha: this.formGroup.value.senha,
    }

    this.usuarioService.salvar(this.usuario);

    // this.usuarioService.excluir(this.usuarioService.buscarPorId(this.usuario.id));
    // this.usuarioService.autenticar(this.usuario.email, this.usuario.senha);

    this.exibirMensagem("Registro salvo com sucesso");
    this.navController.navigateBack('/inicio');
  }

  async exibirMensagem(mensagem){
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500,
    })

    toast.present();
  }

}
