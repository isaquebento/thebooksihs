import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { CategoriaService } from 'src/app/services/categoria.service';
import { LivroService } from 'src/app/services/livro.service';

@Component({
  selector: 'app-livro',
  templateUrl: './livro.page.html',
  styleUrls: ['./livro.page.scss'],
})
export class LivroPage implements OnInit {
  livros:any = [];
  categorias:any = [];
  public formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, public alertController: AlertController, public toastController: ToastController, public livroService: LivroService, public navController: NavController, public menuCtrl: MenuController, private categoriaService: CategoriaService){
    this.menuCtrl.enable(true);
    this.formGroup = this.formBuilder.group({
      'categoria':['', Validators.compose([
        Validators.required,
      ])],
    })
  }

  ngOnInit() {
    this.limparIdEditar();
    this.limparFiltrarIdCategoria();
  }

  async ionViewWillEnter(){
    this.livros = this.livroService.listar();
    this.categorias = this.categoriaService.listar();
  }

  async excluirLivro(id: number){
    let livro = this.livroService.buscarPorId(id);
    
    const alert = await this.alertController.create({
      header: 'Confirma a exclusão?',
      message: livro.descricao,
      buttons: [
        {
          text: "Cancelar"
        }, {
          text: "Confirmar",
          cssClass: 'danger',
          handler: () => {
            this.livroService.excluir(livro);
            this.livros = this.livroService.listar();
            this.exibirMensagem();
          }
        }
      ]
    });
    await alert.present();
  }

  async submitForm(){
    localStorage.setItem('idFiltrarCategoria', this.formGroup.value.categoria);
    this.navController.navigateBack('/listar-categoria');
  }

  async exibirMensagem(){
    const toast = await this.toastController.create({
      message: 'Registro excluído com sucesso',
      duration: 1500,
    });
    toast.present();
  }

  async editarLivro(id: number) {
    localStorage.setItem('idEditarLivro', JSON.stringify(id));
    this.navController.navigateBack('/add-livro');
  }

  async limparIdEditar(){
    this.livroService.limparIdEditar();
  }

  async limparFiltrarIdCategoria(){
    localStorage.setItem('idFiltrarCategoria', JSON.stringify(-1));
  }
}
