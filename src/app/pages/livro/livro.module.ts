import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivroPageRoutingModule } from './livro-routing.module';

import { LivroPage } from './livro.page';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivroPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [LivroPage, HeaderComponent]
})
export class LivroPageModule {}
