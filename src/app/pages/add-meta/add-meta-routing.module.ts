import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddMetaPage } from './add-meta.page';

const routes: Routes = [
  {
    path: '',
    component: AddMetaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddMetaPageRoutingModule {}
