import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { MetaService } from 'src/app/services/meta.service';

@Component({
  selector: 'app-add-meta',
  templateUrl: './add-meta.page.html',
  styleUrls: ['./add-meta.page.scss'],
})
export class AddMetaPage implements OnInit {
  id: string;
  meta = {
    id: null,
    dataInicio: null,
    dataInicioFormat: null,
    dataFinal: null,
    dataFinalFormat: null,
    livrosTotal: null,
    livrosLidos: null,
    usuario: null,
    classe: null,
  }

  public formGroup: FormGroup;

  constructor(private navController: NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private formBuilder: FormBuilder, private metaService: MetaService, public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
    this.formGroup = this.formBuilder.group({
      'dataInicio':['', Validators.compose([
        Validators.required,
      ])],
      'dataFinal':['', Validators.compose([
        Validators.required,
      ])],
      'livrosTotal':['', Validators.compose([
        Validators.required,
      ])],
    });
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if(parseInt(this.metaService.recuperarIdEditar()) != -1) {
      this.id = this.metaService.recuperarIdEditar();
    }
    if(this.id) {
      this.meta = this.metaService.buscarPorId(parseInt(this.id));
      this.formGroup.get('dataInicio').setValue(this.meta.dataInicio);
      this.formGroup.get('dataFinal').setValue(this.meta.dataFinal);
      this.formGroup.get('livrosTotal').setValue(this.meta.livrosTotal);
    }
  }

  async submitForm() {
    this.meta.livrosTotal = this.formGroup.value.livrosTotal;

    if (this.meta.livrosTotal <= 0){
      this.exibirMensagem('Falha ao salvar registro');
      this.navController.navigateBack('/meta');
      return;
    }

    this.meta.usuario = null;

    this.meta.dataFinal = new Date(this.formGroup.value.dataFinal);
    let finalDate = this.formGroup.value.dataFinal.split('-');
    this.meta.dataFinalFormat = finalDate[2] + '/' + finalDate[1] + '/' + finalDate[0];

    this.meta.dataInicio = new Date(this.formGroup.value.dataInicio);
    let initialDate = this.formGroup.value.dataInicio.split('-');
    this.meta.dataInicioFormat = initialDate[2] + '/' + initialDate[1] + '/' + initialDate[0];

    if (this.meta.dataFinal - this.meta.dataInicio <= 0){
      this.exibirMensagem('Falha ao salvar registro');
      this.navController.navigateBack('/meta');
      return;
    }

    this.meta.livrosLidos = 0;
    this.meta.classe = "";
    
    this.metaService.salvar(this.meta);
    this.exibirMensagem('Meta salva!');
    this.navController.navigateBack('/meta');
  }

  async exibirMensagem(mensagem: string) {
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500
    });
    toast.present();
  }
}
