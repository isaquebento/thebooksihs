import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddMetaPageRoutingModule } from './add-meta-routing.module';

import { AddMetaPage } from './add-meta.page';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddMetaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddMetaPage, HeaderComponent]
})
export class AddMetaPageModule {}
