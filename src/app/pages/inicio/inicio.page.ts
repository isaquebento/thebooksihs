import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  public appPages = [
    { title: 'Categorias', url: '/categoria', icon: 'document', color:"dark" },
    { title: 'Livros', url: '/livro', icon: 'document-text', color:"dark" },
    { title: 'Metas', url: '/meta', icon: 'checkmark', color:'dark'},
    { title: 'Meus dados', url: '/usuario', icon: 'man', color:"dark" },
    { title: 'Sair', url: '/login', icon: 'log-out', color:"danger" },
  ];

  constructor(public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
  }

  ngOnInit() {
  }

}
