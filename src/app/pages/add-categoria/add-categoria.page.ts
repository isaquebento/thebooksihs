import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-add-categoria',
  templateUrl: './add-categoria.page.html',
  styleUrls: ['./add-categoria.page.scss'],
})
export class AddCategoriaPage implements OnInit {
  id: string;
  categoria = {
    id: null,
    nome: null,
    usuario: null,
  }

  public formGroup: FormGroup;

  constructor(private navController: NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private formBuilder: FormBuilder, private categoriaService: CategoriaService, public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
    this.formGroup = this.formBuilder.group({
      'nome':['', Validators.compose([
        Validators.required,
      ])]
    });
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if(parseInt(this.categoriaService.recuperarIdEditar()) != -1) {
      this.id = this.categoriaService.recuperarIdEditar();
    }
    if(this.id) {
      this.categoria = this.categoriaService.buscarPorId(parseInt(this.id));
      this.formGroup.get('nome').setValue(this.categoria.nome);
    }
  }

  async submitForm() {
    this.categoria.nome = this.formGroup.value.nome;
    this.categoria.usuario = null;
    this.categoriaService.salvar(this.categoria);
    this.exibirMensagem('Categoria salva!');
    this.navController.navigateBack('/categoria');
  }

  async exibirMensagem(mensagem: string) {
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500
    });
    toast.present();
  }

}
