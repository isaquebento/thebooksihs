import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCategoriaPageRoutingModule } from './add-categoria-routing.module';

import { AddCategoriaPage } from './add-categoria.page';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCategoriaPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [AddCategoriaPage, HeaderComponent]
})
export class AddCategoriaPageModule {}
