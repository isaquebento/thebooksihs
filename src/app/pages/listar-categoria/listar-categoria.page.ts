import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { CategoriaService } from 'src/app/services/categoria.service';
import { LivroService } from 'src/app/services/livro.service';

@Component({
  selector: 'app-listar-categoria',
  templateUrl: './listar-categoria.page.html',
  styleUrls: ['./listar-categoria.page.scss'],
})
export class ListarCategoriaPage implements OnInit {
  livros:any = [];
  categoria: any;

  constructor(private livroService: LivroService, private categoriaService: CategoriaService, private menuCtrl: MenuController, private alertController: AlertController, private toastController: ToastController, private navController: NavController){
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.categoria = this.categoriaService.buscarPorId(JSON.parse(localStorage.getItem('idFiltrarCategoria')));
    this.livros = this.livroService.listarPorCategoria(this.categoria);
  }

  async excluirLivro(id: number){
    let livro = this.livroService.buscarPorId(id);
    
    const alert = await this.alertController.create({
      header: 'Confirma a exclusão?',
      message: livro.descricao,
      buttons: [
        {
          text: "Cancelar"
        }, {
          text: "Confirmar",
          cssClass: 'danger',
          handler: () => {
            this.livroService.excluir(livro);
            this.livros = this.livroService.listar();
            this.exibirMensagem();
          }
        }
      ]
    });
    await alert.present();
  }

  async exibirMensagem(){
    const toast = await this.toastController.create({
      message: 'Registro excluído com sucesso',
      duration: 1500,
    });
    toast.present();
  }

  async editarLivro(id: number) {
    localStorage.setItem('idEditarLivro', JSON.stringify(id));
    this.navController.navigateBack('/add-livro');
  }
}
