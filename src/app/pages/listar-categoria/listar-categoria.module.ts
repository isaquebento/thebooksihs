import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarCategoriaPageRoutingModule } from './listar-categoria-routing.module';

import { ListarCategoriaPage } from './listar-categoria.page';


import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarCategoriaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ListarCategoriaPage, HeaderComponent]
})
export class ListarCategoriaPageModule {}
