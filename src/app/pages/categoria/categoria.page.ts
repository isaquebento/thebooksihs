import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.page.html',
  styleUrls: ['./categoria.page.scss'],
})
export class CategoriaPage implements OnInit {
  categorias: any = [];

  constructor(public alertController: AlertController, public toastController: ToastController, public categoriaService: CategoriaService, public navController: NavController,public menuCtrl: MenuController){
    this.menuCtrl.enable(true);
  }
  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.categorias = this.categoriaService.listar();
  }

  async excluirCategoria(id: number) {
    let categoria = this.categoriaService.buscarPorId(id);

    const alert = await this.alertController.create({
      header: 'Quer mesmo excluir?',
      message: categoria.nome,
      buttons: [
        {
          text: 'Cancelar'
        }, {
          text: 'Quero',
          cssClass: 'danger',
          handler: () => {
            this.categoriaService.excluir(categoria);
            this.categorias = this.categoriaService.listar();
            this.exibirMensagem();
          }
        }
      ]
    });
    await alert.present();
  }

  async exibirMensagem() {
    const toast = await this.toastController.create({
      message: 'Categoria excluída com sucesso!',
      duration: 1500
    });
    toast.present();
  }

  async editarCategoria(id: number) {
    localStorage.setItem('idEditarCategoria', JSON.stringify(id));
    this.navController.navigateBack('/add-categoria');
  }
}
