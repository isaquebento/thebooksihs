import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-add-usuario',
  templateUrl: './add-usuario.page.html',
  styleUrls: ['./add-usuario.page.scss'],
})
export class AddUsuarioPage implements OnInit {
  usuario = {
    id:null,
    nome:null,
    cpf:null,
    data:null,
    cel:null,
    email:null,
    senha:null,
    sexo:null,
  }
  public formGroup: FormGroup;

  constructor(private navController: NavController, private activatedRoute: ActivatedRoute, public toastController: ToastController, private formBuilder: FormBuilder, private usuarioService: UsuarioService, public menuCtrl: MenuController){
    this.menuCtrl.enable(false);
    let dataLimite = new Date();
    console.log(dataLimite);
    dataLimite.setFullYear(dataLimite.getFullYear()-100);
    console.log(dataLimite);

    this.formGroup = this.formBuilder.group({
      'nome':['', Validators.compose([
        Validators.required
      ])],
      'cpf':['', Validators.compose([
        Validators.required
      ])],
      'data':['', Validators.compose([
        Validators.required
      ])],
      'cel':['', Validators.compose([
        Validators.required
      ])],
      'email':['', Validators.compose([
        Validators.required, Validators.email
      ])],
      'senha':['', Validators.compose([
        Validators.required
      ])],
      'sexo':['', Validators.compose([
        Validators.required
      ])],
    })
  }

  ngOnInit() {
  }

  async submitForm(){
    this.usuario.nome = this.formGroup.value.nome;
    this.usuario.cpf = this.formGroup.value.cpf;
    this.usuario.data = this.formGroup.value.data;
    let dataNasc:any = new Date(this.usuario.data);
    let dataLimite:any = new Date();
    let agora:any = new Date();
    dataLimite.setFullYear(dataLimite.getFullYear()-100);
    console.log(this.usuario.data+":"+dataLimite+":"+(dataNasc - dataLimite)+":"+(dataLimite - dataNasc));

    if (!(dataNasc - dataLimite >= 0 && dataNasc - agora <= 0)){
      this.exibirMensagem('Falha ao salvar registro');
      this.navController.navigateBack('/login');
      return;
    }

    this.usuario.cel = this.formGroup.value.cel;
    this.usuario.email = this.formGroup.value.email;
    this.usuario.senha = this.formGroup.value.senha;
    this.usuario.sexo = this.formGroup.value.sexo;
    let usuarios = this.usuarioService.listar();
    for (let i = 0; i < usuarios.length; i++){
      if (this.usuario.email == usuarios[i].email || this.usuario.cpf == usuarios[i].cpf){
        this.exibirMensagem('Falha ao salvar registro');
        this.navController.navigateBack('/login');
        return;
      }
    }
    this.usuarioService.salvar(this.usuario);
    this.exibirMensagem("Registro salvo com sucesso");
    this.navController.navigateBack('/login');
  }

  async exibirMensagem(mensagem){
    const toast = await this.toastController.create({
      message: mensagem,
      duration: 1500,
    });
    toast.present();
  }

}
